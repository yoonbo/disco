#include "chroma.h"
#include "actions/ferm/invert/syssolver_linop_aggregate.h"
#include "meas/glue/mesfield.h"
#include "util/ft/sftmom.h"
#include "binaryRecursiveColoring_v2.h"

#include "gamma_ops.h"  // definition of gamma_ops() ftn
#include <iomanip>      // std::setw
#include <algorithm>
#include <fstream>

#define NUM_G     16

//#define CALC_ERR_ERR

#define POW2(a) ((a) * (a))
#define POW3(a) ((a) * (a) * (a))
#define POW4(a) ((a) * (a) * (a) * (a))

using namespace std;
using namespace QDP;
using namespace Chroma;


// Anonymous namespace
namespace TSF
{
  // Standard Time Slicery
  class TimeSliceFunc : public SetFunc
  {
  public:
    TimeSliceFunc(int dir): dir_decay(dir) {}
    int operator() (const multi1d<int>& coordinate) const {return coordinate[dir_decay];}
    int numSubsets() const {return Layout::lattSize()[dir_decay];}
    int dir_decay;
  private:
    TimeSliceFunc() {}  // hide default constructor
  };
}

//====================================================================
// Structures for input parameters
//====================================================================

struct Inverter_t
{
  GroupXML_t  invParamLP;
  GroupXML_t  invParamLP_1;
  GroupXML_t  fermactLP;
  Real        mass;
  bool        setupParamLP_1;
};

struct NoiseSource_t
{
  int version;

  // for Hadamard
  int                   N_Hadamard;
};

struct Checkpoint_t
{
  int version;
  multi1d<int>     checkpoints;
  std::string      OutFileName;
};

struct Params_t
{
  multi1d<int>          nrow;
  Inverter_t            inverter;
  NoiseSource_t         noise_src;
  multi1d<Checkpoint_t> checkpoint;
  int                   mom2_max;
};

struct Inline_input_t
{
  Params_t        param;
  GroupXML_t      cfg;
  QDP::Seed       rng_seed;
};

//====================================================================
// Read input parameters
//====================================================================
void read(XMLReader& xml, const std::string& path, Inverter_t& p)
{
  XMLReader paramtop(xml, path);
  p.invParamLP = readXMLGroup(paramtop, "InvertParamLP", "invType");
  read(paramtop, "Mass", p.mass);

  // Fermion action for Low Precision Inverter
  if (paramtop.count("FermionActionLP") > 0) 
    p.fermactLP = readXMLGroup(paramtop, "FermionActionLP", "FermAct");
  else
    p.fermactLP = readXMLGroup(paramtop, "FermionAction", "FermAct");
 
  // Inverter parameters for the first run of the inversions
  // For Multigrid inverter, the first inversion is setting up the inverter
  if (paramtop.count("InvertParamLP_1") > 0) {
    p.invParamLP_1 = readXMLGroup(paramtop, "InvertParamLP_1", "invType");
    p.setupParamLP_1 = true;
  }
  else {
    p.setupParamLP_1 = false;
  }
}

void read(XMLReader& xml, const std::string& path, NoiseSource_t& p)
{
  XMLReader paramtop(xml, path);

  // Read input style version 
  if (paramtop.count("version") > 0) {
    read(paramtop, "version", p.version);
  }
  else {
    p.version = 1; 
  }

  // Hadamard vectors
  if (paramtop.count("Hadamard") > 0) {
    read(paramtop, "Hadamard/N_Hadamard", p.N_Hadamard);
  }
  else {
    p.N_Hadamard = 0;
  }
}

void read(XMLReader& xml, const std::string& path, Checkpoint_t& p)
{
  XMLReader paramtop(xml, path);

  // Read input style version 
  if (paramtop.count("version") > 0) {
    read(paramtop, "version", p.version);
  }
  else {
    p.version = 1; 
  }

  read(paramtop, "Checkpoints", p.checkpoints);
  read(paramtop, "OutFile", p.OutFileName);
}

void read(XMLReader& xml, const std::string& path, Params_t& p)
{
  XMLReader paramtop(xml, path);
  read(paramtop, "nrow", p.nrow);

  if (paramtop.count("mom2_max") > 0)
    read(paramtop, "mom2_max", p.mom2_max);
  else
    p.mom2_max = 0;

  read(paramtop, "Inverter",    p.inverter);
  read(paramtop, "NoiseSource", p.noise_src);
  read(paramtop, "Checkpoint",  p.checkpoint);
}

void read(XMLReader& xml, const std::string& path, Inline_input_t& p)
{
  try {
    XMLReader paramtop(xml, path);

    read(paramtop, "Param",       p.param);
    p.cfg = readXMLGroup(paramtop, "Cfg", "cfg_type");
  
    if (paramtop.count("RNG") > 0)
      read(paramtop, "RNG", p.rng_seed);
    else
      p.rng_seed = 11;     // default seed 
  }
  catch( const std::string& e )
  {
    QDPIO::cerr << "Error reading XML : " << e << endl;
    QDP_abort(1);
  }
}

bool linkageHack(void)
{
  bool success = true;

  success &= GaugeInitEnv::registerAll();
  success &= WilsonTypeFermActsEnv::registerAll();
  success &= LinOpSysSolverEnv::registerAll();

  return success;
}

//====================================================================
// Calculate statistical error, and save results 
//====================================================================

void checkout(multi3d<DComplex> &TrM_inv, multi3d<int> &TrM_inv_counter, int N_HV, std::string out_fname, int NumMom, int NumTs, SftMom phases)
{
  if(NumMom != phases.numMom())
  {
    QDPIO::cerr << "Error! Inconsistent NumMom" << NumMom << " != " << phases.numMom()  << endl;
    QDP_abort(1);
  }

  multi3d<DComplex> TrM_inv_av(NUM_G, NumMom, NumTs);

  for(int g=0; g<NUM_G;  ++g)
  for(int p=0; p<NumMom; ++p)
  for(int t=0; t<NumTs;  ++t)
  {
    // 12 for spin/color dilution
    if(TrM_inv_counter[g][p][t] != N_HV*12)
    {
      QDPIO::cerr << "Unexpected TrM_inv_counter value " << g << ", " << p << ", " << t << ": " << TrM_inv_counter[g][p][t] << endl;
      QDP_abort(1);
    }

    TrM_inv_av[g][p][t] = TrM_inv[g][p][t] / N_HV;
  }

  //====================================================
  // Save in text format
  //====================================================
  char buffer[250];
  sprintf(buffer, "%s_%03d", out_fname.c_str(), N_HV);
  std::string out_fname_c(buffer);

  TextFileWriter fout(out_fname_c+".txt");

  // Print header block
  {
    fout << "# NumMom NumT NumOps" << "\n" ;
    char buffer[250];
    sprintf(buffer, "%d %d %d\n", NumMom, NumTs, NUM_G);
    std::string oline(buffer);
    fout << oline;
  }

  fout << "# px py pz  t   g  Tr[M^-1 g_i]_re  Tr[M^-1 g_i]_im   StatErr_re      StatErr_im" << "\n" ;
  
  for(int p=0; p<NumMom; ++p) {
    multi1d<int> pp = phases.numToMom(p);

    for(int t=0; t<NumTs; ++t) {

      for(int g=0; g<NUM_G; ++g){
        char buffer[250];
        sprintf(buffer, "%2d %2d %2d %3d %2d %20.12e %20.12e %20.12e %20.12e\n",
          pp[0], pp[1], pp[2],
          t,
          g,
          TrM_inv_av[g][p][t].elem().elem().elem().real(),
          TrM_inv_av[g][p][t].elem().elem().elem().imag(),
          0.0,
          0.0);
        std::string oline(buffer);
        fout << oline;
      } // for(int g=0; g<NUM_G; ++g)
  
    } // for(int t=0; t<NumTs; ++t)
  } // for(int p=0; p<NumMom; ++p)
  
  fout.close();

} // double checkout()


//====================================================================
// Main program
//====================================================================
int main(int argc, char **argv)
{
  // Put the machine into a known state
  Chroma::initialize(&argc, &argv);
  
  // Put this in to enable profiling etc.
  START_CODE();

  QDPIO::cout << "Linkage = " << linkageHack() << endl;

  StopWatch snoop;
  snoop.reset();
  snoop.start();

  // Instantiate xml reader for DATA
  // if you used -i input file, Chroma::getXMLInputFileName() returns
  //  the filename you specified
  // otherwise it will return "DATA"
  XMLReader xml_in;
  Inline_input_t  input;

  try
  {
    xml_in.open(Chroma::getXMLInputFileName());
    read(xml_in, "/disco", input);
  }
  catch(const std::string& e)
  {
    QDPIO::cerr << "DISCO: Caught Exception reading XML: " << e << endl;
    QDP_abort(1);
  }
  catch(std::exception& e)
  {
    QDPIO::cerr << "DISCO: Caught standard library exception: " << e.what() << endl;
    QDP_abort(1);
  }
  catch(...)
  {
    QDPIO::cerr << "DISCO: caught generic exception reading XML" << endl;
    QDP_abort(1);
  }

  XMLFileWriter& xml_out = Chroma::getXMLOutputInstance();
  push(xml_out, "disco");

  // Write out the input
  write(xml_out, "Input", xml_in);

  Layout::setLattSize(input.param.nrow);
  Layout::create();

  proginfo(xml_out);    // Print out basic program info

  // Initialize the RNG
  QDP::RNG::setrn(input.rng_seed);
  write(xml_out,"RNG", input.rng_seed);

  // Initialize stop watch
  StopWatch swatch;

  // Start up the config
  swatch.reset();
  multi1d<LatticeColorMatrix> u(Nd);
  XMLReader gauge_file_xml, gauge_xml;

  // Start up the gauge field
  QDPIO::cout << "Attempt to read gauge field" << endl;
  swatch.start();
  try
  {
    std::istringstream  xml_c(input.cfg.xml);
    XMLReader  cfgtop(xml_c);
    QDPIO::cout << "Gauge initialization: cfg_type = " << input.cfg.id << endl;

    Handle< GaugeInit >
      gaugeInit(TheGaugeInitFactory::Instance().createObject(input.cfg.id,
                   cfgtop,
                   input.cfg.path));
    (*gaugeInit)(gauge_file_xml, gauge_xml, u);
  }
  catch(std::bad_cast)
  {
    QDPIO::cerr << "DISCO: caught cast error" << endl;
    QDP_abort(1);
  }
  catch(std::bad_alloc)
  {
    // This might happen on any node, so report it
    cerr << "DISCO: caught bad memory allocation" << endl;
    QDP_abort(1);
  }
  catch(const std::string& e)
  {
    QDPIO::cerr << "DISCO: Caught Exception: " << e << endl;
    QDP_abort(1);
  }
  catch(std::exception& e)
  {
    QDPIO::cerr << "DISCO: Caught standard library exception: " << e.what() << endl;
    QDP_abort(1);
  }
  catch(...)
  {
    // This might happen on any node, so report it
    cerr << "DISCO: caught generic exception during gaugeInit" << endl;
    QDP_abort(1);
  }
  swatch.stop();

  QDPIO::cout << "Gauge field successfully read: time= "
        << swatch.getTimeInSeconds()
        << " secs" << endl;

  XMLBufferWriter config_xml;
  config_xml << gauge_xml;

  // Write out the config header
  write(xml_out, "Config_info", gauge_xml);


  //====================================================================
  // Prepare variables
  //====================================================================
  QDPIO::cout << "Max momentum square = " << input.param.mom2_max << endl;

  bool setupParamLP_1 = input.param.inverter.setupParamLP_1;
  int N_Hadamard      = input.param.noise_src.N_Hadamard;
  int NumTs           = Layout::lattSize()[3];

  bool UseHV = false;
  if(N_Hadamard > 0) UseHV = true;

  // Momentum for Fourier Transformation
  // mom2_max, avg_equiv_mom, j_decay
  SftMom phases(input.param.mom2_max, false, Nd-1);
  int NumMom = phases.numMom();


  multi1d<int> checkpoints = input.param.checkpoint[0].checkpoints;
  std::string  OutFileName = input.param.checkpoint[0].OutFileName;

  // Machinery to do timeslice sums with 
  Set TS;
  TS.make(TSF::TimeSliceFunc(Nd-1));

  //====================================================================
  // Do Inversion
  //====================================================================
  typedef LatticeFermion               T;
  typedef multi1d<LatticeColorMatrix>  P;

  //
  // Initialize fermion action
  //
  std::istringstream  xml_sLP(input.param.inverter.fermactLP.xml);
  XMLReader  fermacttopLP(xml_sLP);
  QDPIO::cout << "FermAct_LP = " << input.param.inverter.fermactLP.id << endl;

  // Generic Wilson-Type fermion action handles 
  Handle< WilsonTypeFermAct<T,P,P> >
    S_f_LP(TheWilsonTypeFermActFactory::Instance().createObject(input.param.inverter.fermactLP.id,
          fermacttopLP,
          input.param.inverter.fermactLP.path));

  Handle< FermState<T,P,P> > stateLP(S_f_LP->createState(u));

  // Solvers
  Handle< SystemSolver<LatticeFermion> > PP_LP;
  Handle< SystemSolver<LatticeFermion> > PP_LP_1;

  PP_LP   = S_f_LP->qprop(stateLP, input.param.inverter.invParamLP);
  PP_LP_1 = S_f_LP->qprop(stateLP, input.param.inverter.invParamLP_1);
  // PP_HP will be initialized in the loop


  int ZN  = 4;
  LatticeReal rnd1, theta;
  // twopi defined in chroma/lib/chromabase.h
  Real twopiN = Chroma::twopi / ZN; 
  random(rnd1); 
  theta = twopiN * floor(ZN*rnd1);
  LatticeComplex noise_vec = cmplx(cos(theta),sin(theta));

  //Here is where the HP stuff is created; adopted from Andrea's main function. 
  struct meshVars mesh;
  unsigned int i, N, Hpsize, sample;
  unsigned int *perm, *Hperm;
  //double *RHS;

  mesh.d = Nd;
  mesh.ptsPerDim = (unsigned int *)malloc(sizeof(unsigned int)*mesh.d);
  N=1;
  for (i=0;i<mesh.d; i++){
    //printf("dim(%u): ",i);
    //scanf("%u", &mesh.ptsPerDim[i]);
    //This logic is taken from Bit Twiddling Hacks by Sean Anderson, September 5, 2010
    unsigned int v = Layout::lattSize()[i]; // compute the next highest power of 2 of 32-bit v
    v--;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v++;
    //mesh.ptsPerDim[i] = Layout::lattSize()[i];
    //Extend HP to beyond purely powers of two.
    mesh.ptsPerDim[i] = v;
    N *= mesh.ptsPerDim[i];
  }

  /* Set up the hierarchical data structs */
  hierOrderSetUp(&mesh);

  /* Find the row permutation once for each point in local mesh*/
  /* Here it's done for all nodes N. But can be done only for local ones */
  perm = (unsigned int *)malloc(sizeof(unsigned int)*N);
  hierPerm(&mesh, perm, N);

  /* Now with the perm obtained we do not need the mesh any more */
  freeMeshVars(&mesh);

  /* Create the column permutation of the Hadamard vectors. Do it once */
  /* Create only for as many columns as you need. For example 1024 or 4096 */
  /* CAUTION: N is the global size (total spatial dimension of the mesh) */
  /* CAUTION: N must be a power of 2 */
  //Hpsize = 1024;
  Hpsize = N_Hadamard;
  Hperm = (unsigned int *)malloc(sizeof(unsigned int)*Hpsize);
  hadaColPerm(N, Hperm, Hpsize);
  /**********************************************************************/
  /* At this point we are ready to create the permuted Hadamard vectors */
  /* What is needed is perm and Hperm */
  /**********************************************************************/

  //Object to hold HP vectors we want to invert
  multi1d <LatticeInteger> vectors;
  vectors.resize(N_Hadamard);

  //Back to code adopted from Andrea's main function.
  //for (sample=0; sample<Hpsize; sample++) {
  ///* I create all N here, but it can be done on local rows only */
  //for (i=0;i<N;i++) 
  //RHS[i] = (double) Hada_element(perm[i], Hperm[sample]);

  ///* use RHS in trace computation */
  //}

  int Nx = Layout::lattSize()[0];
  int Ny = Layout::lattSize()[1];
  int Nz = Layout::lattSize()[2];
  int Nt = Layout::lattSize()[3];
  for(int HP_index = 1; HP_index < N_Hadamard + 1; HP_index++)
  {
    QDPIO::cout<<"Building HP vector number "<<HP_index<<std::endl;
    for(int x = 0; x < Nx; x++)
    for(int y = 0; y < Ny; y++)
    for(int z = 0; z < Nz; z++)
    for(int t = 0; t < Nt; t++)
    {
      multi1d<int> chroma_coords;
      chroma_coords.resize(Nd);
      chroma_coords[0] = x; chroma_coords[1] = y; chroma_coords[2] = z; chroma_coords[3] = t;
      int i = Layout::linearSiteIndex(chroma_coords);
      int element = Hada_element(perm[i], (Hperm[HP_index - 1]));
      Integer chroma_element = element;
      pokeSite(vectors[HP_index-1], chroma_element, chroma_coords);
    }
  }


  multi3d<DComplex> TrM_inv(NUM_G, NumMom, NumTs);
  multi3d<int>      TrM_inv_counter(NUM_G, NumMom, NumTs);

  for(int g=0; g<NUM_G; ++g)
  for(int p=0; p<NumMom; ++p)
  for(int t=0; t<NumTs; ++t)
  {
    TrM_inv[g][p][t] = zero;
    TrM_inv_counter[g][p][t] = 0;
  }

  bool inverter_initialized = false;

  //For debugging HP vectors.
  ComplexD Trace = 0.0;
  for(int vec_index = 0; vec_index < vectors.size(); vec_index++)
  {
    QDPIO::cout<<"Inverting hierarchnical probing vector number "<<vec_index+1<<std::endl;
    LatticePropagator noise_prop = zero;
    //Now let's do some dilution...
    for(int color_source(0);color_source<Nc;color_source++){
      QDPIO::cout << "color_source = " << color_source << std::endl; 

      LatticeColorVector vec_srce = zero ;
      LatticeComplex HP_vec = noise_vec*vectors[vec_index];
      pokeColor(vec_srce,HP_vec,color_source) ;

      for(int spin_source=0; spin_source < Ns; ++spin_source){
        QDPIO::cout << "spin_source = " << spin_source << std::endl; 

        // Insert a ColorVector into spin index spin_source
        // This only overwrites sections, so need to initialize first
        // Also, do the solve here
        LatticeFermion chi = zero;
        LatticeFermion psi = zero;
        CvToFerm(vec_srce, chi, spin_source);

        LatticeFermion eta = chi;
        // Calculate psi by using Dirac Inverter with Low Precision
        SystemSolverResults_t res;
        if(!inverter_initialized) {
          // When the inverter runs at the first time, it may use different input
          // parameters, especially in the multigrid solver
          res = (*PP_LP_1)(psi, eta);
        }
        else {
          res = (*PP_LP)(psi, eta);
        }

        // Calculate Tr(M^-1) = (1/N) sum_i <eta_i| \gamma |psi_i>
        for(int g=0; g<NUM_G; ++g) {
          LatticeComplex corr_fn = localInnerProduct(chi, gamma_ops(g) * psi);

          //corr_fn_t = sumMulti(corr_fn, TS);
          multi2d<DComplex> corr_fn_t = phases.sft(corr_fn);

          for(int p=0; p<NumMom; ++p)
          for(int t=0; t<NumTs; ++t)
          {
            TrM_inv[g][p][t] += corr_fn_t[p][t];
            TrM_inv_counter[g][p][t] += 1;
          }

        } // for(int g=0; g<NUM_G; ++g)

      } // for(spin_source)
    } // for(color_source)

    //check checkpoints
    for(int i=0; i<checkpoints.size(); ++i)
      if(vec_index+1 == checkpoints[i]) checkout(TrM_inv, TrM_inv_counter, vec_index+1, OutFileName, NumMom, NumTs, phases);


  } // for(vec_index)

  xml_out.flush();
  pop(xml_out);

  //-----------------------------
  // End program 
  //-----------------------------
  snoop.stop();
  QDPIO::cout << "DISCO: total time = "
    << snoop.getTimeInSeconds()
    << " secs" << endl << flush;

  QDPIO::cout << "DISCO: ran successfully" << endl;

  END_CODE();

  Chroma::finalize();
  exit(0);
}
