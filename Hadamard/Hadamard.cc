#include "chroma.h"
#include "binaryRecursiveColoring_v2.h"
#include<cassert>

using namespace std;
using namespace QDP;
using namespace Chroma;


//====================================================================
// Main program
//====================================================================
int main(int argc, char *argv[])
{
  // Put the machine into a known state
  Chroma::initialize(&argc, &argv);

  // Put this in to enable profiling etc.
  START_CODE();


  StopWatch snoop;
  snoop.reset();
  snoop.start();

  if(argc != 8) {
    cerr << "Usage : " << argv[0]
      << " NX NY NZ NT StarVectorIdx  EndVectorIdx  OutputDirectory" << endl;
    // StarVectorIdx = 1,2,3,4,...
    // EndVectorIdx  = 1,2,3,4,...
    QDP_abort(1);
  }

  int nx, ny, nz, nt;
  sscanf(argv[1],"%d",&nx);
  sscanf(argv[2],"%d",&ny);
  sscanf(argv[3],"%d",&nz);
  sscanf(argv[4],"%d",&nt);

  multi1d<int> latt_size(Nd);
  latt_size[0] = nx;
  latt_size[1] = ny;
  latt_size[2] = nz;
  latt_size[3] = nt;

  QDPIO::cout << "Lattice size (nx, ny, nz, nt) = ("
    << nx << " " << ny << " "
    << nz << " " << nt << ")" << endl;

  int starting_vector, ending_vector;
  sscanf(argv[5],"%d",&starting_vector);
  sscanf(argv[6],"%d",&ending_vector);

  QDPIO::cout<<"Starting and ending vector indices"<<" "<<starting_vector<<","<<ending_vector<<endl;
  
  if(starting_vector < 1 || ending_vector < starting_vector)
  {
    QDPIO::cerr << "Error(Hadamard): StarVectorIdx should be greater than 0, and EndVectorIdx should be equal or greater than StarVectorIdx.";
    QDP_abort(1);
  }

  string out_dir = argv[7];
  QDPIO::cout<<"Output directory = " << out_dir << std::endl;


  //====================================================================
  // Prepare variables
  //====================================================================

  Layout::setLattSize(latt_size);
  Layout::create();   // Setup the layout


  //Creating Hadamard vectors

  struct meshVars mesh;
  index_t i, N, Hpsize, sample;
  index_t *perm, *Hperm;
  mesh.d = Nd;
  mesh.ptsPerDim = (index_t *)malloc(sizeof(index_t)*mesh.d);
  N=1;

  //Finding the next power of 2 if lattice size in any directions is not a power of 2 

  for (i=0;i<mesh.d; i++){
    index_t v = Layout::lattSize()[i];
    v--;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v++;
    mesh.ptsPerDim[i] = v;
    N *= mesh.ptsPerDim[i];
  }

  int Nxh =mesh.ptsPerDim[0];
  int Nyh =mesh.ptsPerDim[1];
  int Nzh =mesh.ptsPerDim[2];
  int Nth =mesh.ptsPerDim[3];
  QDPIO::cout<<Nxh<<Nyh<<Nzh<<Nth<<endl;
 
  QDPIO::cout << "Hadamard mesh size (nxh, nyh, nzh, nth), N = ("
    << Nxh << " " << Nyh << " "
    << Nzh << " " << Nth << "), " << N << endl;


  /* Set up the hierarchical data structs */

  hierOrderSetUp(&mesh);

  /* Find the row permutation once for each point in local mesh*/
  /* Here it's done for all nodes N. But can be done only for local ones */


  perm = (index_t *)malloc(sizeof(index_t)*N);
  hierPerm(&mesh, perm, N);

  /* Now with the perm obtained we do not need the mesh any more */
  freeMeshVars(&mesh);


  /* Create the column permutation of the Hadamard vectors. Do it once */
  /* Create only for as many columns as you need. For example 1024 or 4096 */
  /* CAUTION: N is the global size (total spatial dimension of the mesh) */
  /* CAUTION: N must be a power of 2 */
  //Hpsize = 1024;

  Hpsize = ending_vector;
  Hperm = (index_t *)malloc(sizeof(index_t)*Hpsize);
  hadaColPerm(N, Hperm, Hpsize);
  /**********************************************************************/
  /* At this point we are ready to create the permuted Hadamard vectors */
  /* What is needed is perm and Hperm */
  /**********************************************************************/

  // Take the relevant part of the Hadamard vectors 
  LatticeInteger hvector;

  int Nx = Layout::lattSize()[0];
  int Ny = Layout::lattSize()[1];
  int Nz = Layout::lattSize()[2];
  int Nt = Layout::lattSize()[3];
  for(int HP_index = starting_vector; HP_index < ending_vector + 1; HP_index++)
  {
    hvector = zero;

    // Calculate Hadamard vector
    QDPIO::cout<<"Building HP vector number "<<HP_index<<std::endl;
    for(int x = 0; x < Nx; x++)
    for(int y = 0; y < Ny; y++)
    for(int z = 0; z < Nz; z++)
    for(int t = 0; t < Nt; t++)
    {
      multi1d<int> chroma_coords;
      chroma_coords.resize(Nd);
      chroma_coords[0] = x; chroma_coords[1] = y; chroma_coords[2] = z; chroma_coords[3] = t;
      index_t i = Layout::linearSiteIndex(chroma_coords);
      int element = Hada_element(perm[i], (Hperm[HP_index - 1]));
      Integer chroma_element = element;
      pokeSite(hvector, chroma_element, chroma_coords);
    }

    hvector *= 2;
    QDPIO::cout<< "!!WARNING!!!!! Hadamard vectors are multiplied by 2!" << std::endl;
    // It looks like the file writer does not write out checksums properly when
    // the data (lattce integer) meets certain condition. To avoid the issue,
    // as a dirty workaround, we multiply 2

    // Save Hadamard vector
    char buf[4000];
    sprintf(buf, "%s/HV_%d_%d_%d_%d-idx%05d.dat", out_dir.c_str(), nx, ny, nz, nt, HP_index);
    string out_fname = buf;

    int data_version = 1;

    // Output file header
    XMLBufferWriter file_xml;
    push(file_xml, "HadamardVector");
    write(file_xml, "Version", data_version);
    write(file_xml, "LatticeLayout", Layout::lattSize());
    write(file_xml, "HadamardIndex", HP_index);
    pop(file_xml);

    XMLBufferWriter rec_xml;
    push(rec_xml, "HadamardVector");
    write(rec_xml, "Version", data_version);
    write(rec_xml, "LatticeLayout", Layout::lattSize());
    write(rec_xml, "HadamardIndex", HP_index);
    pop(rec_xml);

    QDPFileWriter bin_out(file_xml, out_fname, QDPIO_SINGLEFILE, QDPIO_SERIAL, QDPIO_OPEN);
    write(bin_out, rec_xml, hvector);
    close(bin_out);

    // Read test
    {
      XMLReader file_xml, rec_xml;
      QDPFileReader bin_reader(file_xml, out_fname, QDPIO_SERIAL);
      int data_version, read_i_hv;
      read(file_xml, "/HadamardVector/Version", data_version);
      read(file_xml, "/HadamardVector/HadamardIndex", read_i_hv);

      LatticeInteger hvec_read;
      bin_reader.read(rec_xml, hvec_read);
      bin_reader.close();
    }

  }// for(HP_index)


  //-----------------------------
  // End program 
  //-----------------------------
  snoop.stop();
  QDPIO::cout << "Hadamard: total time = "
    << snoop.getTimeInSeconds()
    << " secs" << endl << flush;

  QDPIO::cout << "Hadamard: ran successfully" << endl;

  END_CODE();

  Chroma::finalize();
  exit(0);
}
